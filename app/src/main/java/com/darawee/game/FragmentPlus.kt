package com.darawee.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.darawee.game.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentPlus.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentPlus : Fragment() {
    private lateinit var binding: FragmentPlusBinding
    private var resultText = "Please select an answer"
    private  var scoreNum = Score()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_plus,container,false)
        binding.resultText = resultText
        binding.score = scoreNum
        play()
        binding.apply {
            btnNext.setOnClickListener{
                play()
                txtAns.setText("Please select")
            }
        }
        binding.btnBack.setOnClickListener{
            it.findNavController().navigate(R.id.action_fragmentPlus_to_startMenuFragment)
        }
        return binding.root
    }

    private fun play() {
        binding.apply {
            val random1: Int = Random.nextInt(10) + 1 //generate random numbers from 1-10
            txtNum1.setText(Integer.toString(random1)) //n1

            val random2: Int = Random.nextInt(10) + 1 //generate random numbers from 1-10
            txtNum2.setText(Integer.toString(random2)) //n2

            val sum: Int = random1 + random2
            val index: Int = Random.nextInt(until = 3) + 1
            if (index == 1) {
                btnNum1.setText(Integer.toString(sum))
                btnNum2.setText(Integer.toString(sum + 2))
                btnNum3.setText(Integer.toString(sum + 3))
            }

            if (index == 2) {
                btnNum1.setText(Integer.toString(sum - 2))
                btnNum2.setText(Integer.toString(sum))
                btnNum3.setText(Integer.toString(sum + 2))
            }

            if (index == 3) {
                btnNum1.setText(Integer.toString(sum - 4))
                btnNum2.setText(Integer.toString(sum - 2))
                btnNum3.setText(Integer.toString(sum))
            }

            btnNum1.setOnClickListener {
                if (btnNum1.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                }
                binding.invalidateAll()
            }
            btnNum2.setOnClickListener {
                if (btnNum2.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                }
                binding.invalidateAll()
            }
            btnNum3.setOnClickListener {
                if (btnNum3.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
//                    txtAns.setText("Correct")
////                    txtCorrect.setText((txtCorrect.text.toString().toInt() + 1).toString())
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
//                    txtAns.setText("Wrong")
//                    txtWrong.setText((txtWrong.text.toString().toInt() + 1).toString())
                }
                binding.invalidateAll()
            }


        }

    }
}